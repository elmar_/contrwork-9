import axios from "axios";

const axiosContact = axios.create({
    baseURL: 'https://classwork-63-burger-default-rtdb.firebaseio.com/counterwork-9/'
});

export default axiosContact;