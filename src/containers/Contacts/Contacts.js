import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts, refresh} from "../../store/contactsAction";
import Contact from "../../components/Contact/Contact";
import Spinner from "../../components/Spinner/Spinner";
import Modal from "../../components/Modal/Modal";
import './Contacts.css';

const Contacts = () => {
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts);
    const loading = useSelector(state => state.contactsLoading);
    const error = useSelector(state => state.contactsError);

    const [modal, setModal] = useState(null);


    useEffect(() => {
        dispatch(refresh());
        dispatch(fetchContacts());
    }, [dispatch]);

    const showModal = (contact, removeContact) => {
        setModal(<Modal contact={contact} setModal={setModal} removeContact={removeContact} />);
    };


    let contactsBlock = (contacts.map((contact, index) => (
        <Contact contact={contact} key={contact.id} showModal={showModal} index={index} setModal={setModal} />
    )));


    if (loading) {
        contactsBlock = <Spinner />;
    }

    if (error) {
        contactsBlock = <p>Error</p>;
    }

    return (
        <div className="Contacts">
            {modal}
            {contactsBlock}
        </div>
    );
};

export default Contacts;