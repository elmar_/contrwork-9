import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {edited, editFetch} from "../../store/contactsAction";
import {Redirect} from "react-router-dom";
import Form from "../../components/Form/Form";
import Spinner from "../../components/Spinner/Spinner";
import './EditContact.css';

const EditContact = props => {
    const dispatch = useDispatch();
    const contact = useSelector(state => state.editContact);
    const loading = useSelector(state => state.editLoading);
    const error = useSelector(state => state.editError);
    const contactEdited = useSelector(state => state.edited);

    useEffect(() => {
        dispatch(editFetch(props.match.params.id));
    }, [props.match.params.id, dispatch]);


    const saveEdited = (e, contact, id) => {
        e.preventDefault();

        dispatch(edited(contact, id));
    };

    let formBlock = <Form editContact={contact} formSubmit={saveEdited} id={props.match.params.id} />;

    if (loading) {
        formBlock = <Spinner />;
    }

    if (contactEdited) {
        formBlock = <Redirect to="/" />;
    }

    if (error) {
        formBlock = <p>Error</p>;
    }

    return (
        <div className="EditContact">
            <h3>Edit contact</h3>
            {formBlock}
        </div>
    );
};

export default EditContact;