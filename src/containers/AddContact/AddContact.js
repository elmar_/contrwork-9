import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addContact} from "../../store/contactsAction";
import {Redirect} from "react-router-dom";
import Form from "../../components/Form/Form";
import Spinner from "../../components/Spinner/Spinner";
import './AddContact.css';

const AddContact = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.addLoading);
    const added = useSelector(state => state.contactAdded);
    const error = useSelector(state => state.addError);


    const formSubmit = (e, contact) => {
        e.preventDefault();

        dispatch(addContact(contact));
    };

    let form = <Form formSubmit={formSubmit} />;

    if (loading) {
        form = <Spinner />
    }

    if (added) {
        form = <Redirect to="/" />;
    }

    return (
        <div className="AddContact">
            <h3>Add new contact</h3>
            {error ? <p>Contact was not added (error)</p> : null}
            {form}
        </div>
    );
};

export default AddContact;