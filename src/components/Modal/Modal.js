import React from 'react';
import {Link} from "react-router-dom";
import BackDrop from "../BackDrop/BackDrop";
import './Modal.css';

const Modal = ({contact, setModal, removeContact}) => {
    return (
        <>
            <BackDrop closeModal={setModal} />
            <div className="Modal">
                <div className="info-block">
                    <div className="img-block">
                        <img src={contact.image} alt={contact.name} className="img-modal" />
                    </div>
                    <div>
                        <h3>{contact.name}</h3>
                        <p>{contact.phone}</p>
                        <p>{contact.mail}</p>
                    </div>
                </div>
                <div className="btn-block">
                    <Link to={"/EditContact/" + contact.id} className="modal-btn">Edit</Link>
                    <button className="modal-btn" onClick={removeContact}>Delete</button>
                </div>
            </div>
        </>
    );
};

export default Modal;