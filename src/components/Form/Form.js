import React, {useState} from 'react';
import './Form.css';

const Form = props => {

    const [contact, setContact] = useState(props.editContact || {
        name: '',
        phone: '',
        image: '',
        mail: ''
    });


    const changeInput = e => {
        const {name, value} = e.target;

        setContact({
            ...contact,
            [name]: value
        });
    };

    return (
        <form onSubmit={e => props.formSubmit(e, contact, props.id)}>
            <input
                className="Input" placeholder="Contact name"
                type="text" name="name"
                value={contact.name}
                onChange={changeInput}
            />
            <input
                className="Input" placeholder="Contact phone"
                type="number" name="phone"
                value={contact.phone}
                onChange={changeInput}
            />
            <input
                className="Input" placeholder="Contact mail"
                type="mail" name="mail"
                value={contact.mail}
                onChange={changeInput}
            />
            <input
                className="Input" placeholder="Contact image"
                type="text" name="image"
                value={contact.image}
                onChange={changeInput}
            />
            <div className="img-block"><img src={contact.image} alt={contact.name} className="form-img" /></div>
            <button className="form-btn" type="submit">Save</button>
        </form>
    );
};

export default Form;