import React from 'react';
import './Contact.css';
import {deleteContact} from "../../store/contactsAction";
import {useDispatch} from "react-redux";
import Modal from "../Modal/Modal";

const Contact = ({contact, index, setModal}) => {
    const dispatch = useDispatch();

    const removeContact = () => {
        setModal(null);
        dispatch(deleteContact(contact.id, index));
    };

    return (
        <div className="Contact" onClick={() => setModal(<Modal contact={contact} setModal={setModal} removeContact={removeContact} />)}>
            <img className="contact-img" src={contact.image} alt={contact.name} />
            <h3 className="contact-title">{contact.name}</h3>
        </div>
    );
};

export default Contact;