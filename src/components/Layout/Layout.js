import React from 'react';
import './Layout.css';
import {Link} from "react-router-dom";

const Layout = props => {
    return (
        <div className="Layout">
                <header className="header">
                    <div><Link to="/" className="logo">Contacts</Link></div>
                    <Link className="header-btn" to="AddContact">Add new contact</Link>
                </header>
                {props.children}
        </div>
    );
};

export default Layout;