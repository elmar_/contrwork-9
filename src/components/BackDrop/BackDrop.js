import React from 'react';
import './BackDrop.css';

const BackDrop = ({closeModal}) => {
    return (
        <div className="BackDrop" onClick={() => closeModal(null)}>

        </div>
    );
};

export default BackDrop;