import {
    ADD_CONTACT_ERROR,
    ADD_CONTACT_REQUEST,
    ADD_CONTACT_SUCCESS,
    DELETE_CONTACT_ERROR,
    DELETE_CONTACT_SUCCESS,
    EDIT_FETCH_ERROR, EDIT_FETCH_REQUEST,
    EDIT_FETCH_SUCCESS, EDITED_PUT_ERROR, EDITED_PUT_SUCCESS,
    FETCH_CONTACTS_ERROR,
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS, REFRESH
} from "./contactsAction";

const initialState = {
    contacts: [],
    contactsLoading: false,
    contactsError: null,
    addLoading: false,
    addError: null,
    contactAdded: false,
    deleteError: null,
    editContact: {},
    editLoading: false,
    editError: null,
    edited: false
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_REQUEST:
            return {...state, contactsLoading: true};
        case FETCH_CONTACTS_SUCCESS:
            return {...state, contactsLoading: false, contacts: action.contacts};
        case FETCH_CONTACTS_ERROR:
            return {...state, contactsLoading: false, contactsError: action.error};
        case ADD_CONTACT_REQUEST:
            return {...state, addLoading: true};
        case ADD_CONTACT_SUCCESS:
            return {...state, addLoading: false, contactAdded: true};
        case ADD_CONTACT_ERROR:
            return {...state, addError: action.error};
        case DELETE_CONTACT_SUCCESS:
            const contactsCopy = [...state.contacts];
            contactsCopy.splice(action.index, 1);
            return {...state, contacts: contactsCopy};
        case DELETE_CONTACT_ERROR:
            return {...state, deleteError: action.error};
        case EDIT_FETCH_REQUEST:
            return {...state, editLoading: true};
        case EDIT_FETCH_SUCCESS:
            return {...state, editContact: action.contact, editLoading: false};
        case EDIT_FETCH_ERROR:
            return {...state, editError: action.error, editLoading: false};
        case EDITED_PUT_SUCCESS:
            return {...state, editLoading: false, edited: true};
        case EDITED_PUT_ERROR:
            return {...state, editError: action.error, editLoading: false};
        case REFRESH:
            return initialState;
        default:
            return state;
    }
};

export default contactsReducer;