import axiosContact from "../axios-contact";

export const ADD_CONTACT_REQUEST = 'ADD_CONTACT_REQUEST';
export const ADD_CONTACT_SUCCESS = 'ADD_CONTACT_SUCCESS';
export const ADD_CONTACT_ERROR = 'ADD_CONTACT_ERROR';

export const EDIT_FETCH_REQUEST = 'EDIT_FETCH_REQUEST';
export const EDIT_FETCH_SUCCESS = 'EDIT_FETCH_SUCCESS';
export const EDIT_FETCH_ERROR = 'EDIT_FETCH_ERROR';

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_ERROR = 'FETCH_CONTACTS_ERROR';

export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';
export const DELETE_CONTACT_ERROR = 'DELETE_CONTACT_ERROR';

export const EDITED_PUT_SUCCESS = 'EDITED_PUT_SUCCESS';
export const EDITED_PUT_ERROR = 'EDITED_PUT_ERROR';

export const REFRESH = 'REFRESH';

///////////////////////////////////////////////////////////////////////////


export const editedPutSuccess = () => ({type: EDITED_PUT_SUCCESS});
export const editedPutError = error => ({type: EDITED_PUT_ERROR, error});

export const deleteContactSuccess = index => ({type: DELETE_CONTACT_SUCCESS, index});
export const deleteContactError = error => ({type: DELETE_CONTACT_ERROR, error});

export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = contacts => ({type: FETCH_CONTACTS_SUCCESS, contacts});
export const fetchContactsError = error => ({type: FETCH_CONTACTS_ERROR, error});

export const addContactRequest = () => ({type: ADD_CONTACT_REQUEST});
export const addContactSuccess = () => ({type: ADD_CONTACT_SUCCESS});
export const addContactError = error => ({type: ADD_CONTACT_ERROR, error});

export const editFetchRequest = () => ({type: EDIT_FETCH_REQUEST});
export const editFetchSuccess = contact => ({type: EDIT_FETCH_SUCCESS, contact});
export const editFetchError = error => ({type: EDIT_FETCH_ERROR, error});

export const refresh = () => ({type: REFRESH});

///////////////////////////////////////////////////////////////////////

export const edited = (contact, id) => {
    return async dispatch => {
        try {
            dispatch(editFetchRequest());
            await axiosContact.put('contacts/' + id + '.json', contact);
            console.log(contact)
            dispatch(editedPutSuccess())
            console.log(id)
        } catch (e) {
            dispatch(editedPutError(e));
        }
    };
};


export const editFetch = id => {
    return async dispatch => {
        try {
            dispatch(editFetchRequest());
            const response = await axiosContact.get('contacts/' + id + '.json');
            dispatch(editFetchSuccess(response.data));
        } catch (e) {
            dispatch(editFetchError(e));
        }
    };
};

export const deleteContact = (id, index) => {
    return async dispatch => {
        try {
            await axiosContact.delete('contacts/' + id + '.json');
            dispatch(deleteContactSuccess(index));
        } catch (e) {
            dispatch(deleteContactError(e));
        }
    };
};

export const addContact = contact => {
    return async dispatch => {
        try {
            dispatch(addContactRequest());
            await axiosContact.post('contacts.json', contact);
            dispatch(addContactSuccess());
        } catch (e) {
            dispatch(addContactError(e));
        }
    };
};

export const fetchContacts = () => {
    return async dispatch => {
        try {
            dispatch(fetchContactsRequest());
            const response = await axiosContact.get('contacts.json');
            const contacts = Object.keys(response.data).map(contact => ({
                ...response.data[contact],
                id: contact
            }));
            dispatch(fetchContactsSuccess(contacts));
        } catch (e) {
            dispatch(fetchContactsError(e));
        }
    };
};