import React from 'react';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import AddContact from "./containers/AddContact/AddContact";
import Contacts from "./containers/Contacts/Contacts";
import EditContact from "./containers/EditContact/EditContact";
import './App.css';

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Contacts} />
            <Route path="/AddContact" component={AddContact} />
            <Route path="/EditContact/:id" component={EditContact} />
        </Switch>
    </Layout>
);

export default App;
